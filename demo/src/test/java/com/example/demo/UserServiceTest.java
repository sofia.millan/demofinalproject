package com.example.demo;

import com.example.demo.dto.UserRequestDto;
import com.example.demo.exception.DataAlreadyExistsException;
import com.example.demo.exception.DataNotFoundException;
import com.example.demo.exception.DataNotValidException;
import com.example.demo.model.Role;
import com.example.demo.model.User;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.password.PasswordEncoder;


import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


public class UserServiceTest {

    UserRepository userRepository;
    RoleRepository roleRepository;
    UserService userService;
    PasswordEncoder passwordEncoder;

    @BeforeEach
    public void setUp(){
        this.userRepository = mock(UserRepository.class);
        this.roleRepository = mock(RoleRepository.class);
        this.passwordEncoder = mock(PasswordEncoder.class);
        this.userService = new UserService(userRepository, roleRepository,passwordEncoder);
    }

    @Test
    void mustSaveAUser(){

        Role role = new Role(101L,"Owner","Neque porro quisquam est qui dolorem ipsum " +
                "quia dolor sit amet, consectetur, adipisci velit.");
        UserRequestDto userdto = new UserRequestDto("123","Sofia","Millan","+576542",
                "sofia@gmail.com","123",101L);


        when(roleRepository.findById(role.getId())).thenReturn(Optional.of(role));

        userService.saveUser(userdto);

        verify(userRepository).save(any(User.class));
    }

    @Test
    void Should_Throw_DataNotValidException_When_EmailNotValid(){

        Role role = new Role(101L,"Owner","Neque porro quisquam est qui dolorem ipsum " +
                "quia dolor sit amet, consectetur, adipisci velit.");
        UserRequestDto userdto = new UserRequestDto("123","Sofia","Millan","+576542",
                "sofiagmail.com","123",101L);


        when(roleRepository.findById(role.getId())).thenReturn(Optional.of(role));


        Assertions.assertThrows(
                DataNotValidException.class,
                ()->{
                    userService.saveUser(userdto);
                }
        );
    }
    @Test
    void Should_Throw_DataNotValidException_When_PhoneNumberHasMoreThan13Digits(){

        Role role = new Role(101L,"Owner","Neque porro quisquam est qui dolorem ipsum " +
                "quia dolor sit amet, consectetur, adipisci velit.");
        UserRequestDto userdto = new UserRequestDto("123","Sofia","Millan","+5765421598455845",
                "sofiagmail.com","123",101L);


        when(roleRepository.findById(role.getId())).thenReturn(Optional.of(role));


        Assertions.assertThrows(
                DataNotValidException.class,
                ()->{
                    userService.saveUser(userdto);
                }
        );
    }
    @Test
    void Should_Throw_DataNotValidException_When_DniHasLetters(){

        Role role = new Role(101L,"Owner","Neque porro quisquam est qui dolorem ipsum " +
                "quia dolor sit amet, consectetur, adipisci velit.");
        UserRequestDto userdto = new UserRequestDto("123abc","Sofia","Millan","+57654215",
                "sofiagmail.com","123",101L);


        when(roleRepository.findById(role.getId())).thenReturn(Optional.of(role));


        Assertions.assertThrows(
                DataNotValidException.class,
                ()->{
                    userService.saveUser(userdto);
                }
        );
    }

    @Test
    void Should_Throw_DataNotFoundException_When_IdRoleNotFound(){


        UserRequestDto userdto = new UserRequestDto("123","Sofia","Millan","+576545",
                "sofia@gmail.com","123",101L);

        when(roleRepository.findById(anyLong())).thenReturn(Optional.empty());

        Assertions.assertThrows(
                DataNotFoundException.class,
                ()->{
                    userService.saveUser(userdto);
                }
        );
    }

    @Test
    void Should_Throw_DataAlreadyExistsException_When_EmailAlreadyExists(){

        Role role = new Role(101L,"Owner","Neque porro quisquam est qui dolorem ipsum " +
                "quia dolor sit amet, consectetur, adipisci velit.");

        UserRequestDto userdto = new UserRequestDto("123","Sofia","Millan","+576545",
                "sofia@gmail.com","123",101L);

        when(roleRepository.findById(anyLong())).thenReturn(Optional.of(role));
        when(userRepository.findByEmail(userdto.getEmail())).thenReturn(Optional.of(new User()));
        Assertions.assertThrows(
                DataAlreadyExistsException.class,
                ()->{
                    userService.saveUser(userdto);
                }
        );
    }

}
