package com.example.demo.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name="roles")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Role {
    @Id
    @Column(name = "id_role", nullable = false)
    private Long id;
    @Column(name = "role_name", nullable = false)
    private String name;
    @Column(name = "role_description", nullable = false)
    private String description;

}

