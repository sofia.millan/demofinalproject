package com.example.demo.controller;


import com.example.demo.dto.UserRequestDto;
import com.example.demo.service.UserService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags="User")
@RestController
@RequestMapping("api/v1")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    @ApiOperation(value = "Create a new owner")
    @ApiResponses( value= {
            @ApiResponse(code = 201, message = "Owner created successfully"),
            @ApiResponse(code = 400, message = "Data is not valid, check the input")
    })
    @PostMapping("/owner")
    public ResponseEntity<Void> saveOwner(@ApiParam("Owner's information")@RequestBody UserRequestDto userdto){
        userService.saveUser(userdto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
