package com.example.demo.exception.exceptionhandler;


public enum ExceptionResponse {
    DATA_ALREADY_EXISTS("Some data already exists"),
    DATA_NOT_VALID("Check the format of the data"),
    DATA_NOT_FOUND("Some data was not found in our database");
    private final String message;

    ExceptionResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}

