package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserRequestDto {
    private String dni;
    private String name;
    private String lastName;
    private String phoneNumber;
    private String email;
    private String password;
    private Long id_role;
}
