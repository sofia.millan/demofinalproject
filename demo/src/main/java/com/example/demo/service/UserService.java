package com.example.demo.service;

import com.example.demo.dto.UserRequestDto;
import com.example.demo.exception.DataAlreadyExistsException;
import com.example.demo.exception.DataNotFoundException;
import com.example.demo.exception.DataNotValidException;
import com.example.demo.model.Role;
import com.example.demo.model.User;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, RoleRepository roleRepository,PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }
    public void saveUser(UserRequestDto userdto) {
        if(!validateEmail(userdto.getEmail())){
            throw new DataNotValidException();
        }
        if(!validatePhoneNumber(userdto.getPhoneNumber())){
            throw new DataNotValidException();
        }
        if(!validateOnlyNumbers(userdto.getDni())){
            throw new DataNotValidException();
        }
        Optional<User> optionalUser = userRepository.findByEmail(userdto.getEmail());
        if(optionalUser.isPresent()){
            throw new DataAlreadyExistsException();
        }

        Optional<Role> optionalRole = roleRepository.findById(userdto.getId_role());
        if(optionalRole.isEmpty()){
            throw new DataNotFoundException();
        }
        User user = new User(userdto.getDni(), userdto.getName(), userdto.getLastName(),
                userdto.getPhoneNumber(), userdto.getEmail(),passwordEncoder.encode(userdto.getPassword()), optionalRole.get());
        userRepository.save(user);
    }

    public boolean validateEmail(String email){
        Pattern pattern = Pattern
                .compile("^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher matcher = pattern.matcher(email);
        return matcher.find();
    }

    public boolean validatePhoneNumber(String phoneNumber){
        Pattern pattern = Pattern
                .compile("^(\\(?\\+?[0-9]{2,12}\\)?)$");
        Matcher matcher = pattern.matcher(phoneNumber);
        return matcher.find();
    }

    public boolean validateOnlyNumbers(String string){
        Pattern pattern = Pattern
                .compile("^[0-9]*$");
        Matcher matcher = pattern.matcher(string);
        return matcher.find();
    }

}
